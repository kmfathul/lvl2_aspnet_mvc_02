﻿using System.Web;
using System.Web.Optimization;

namespace LVL2_ASPNet_MVC_02
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/creative").Include(
                        "~/Scripts/creative.min..js"));

            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                        "~/vendor/jquery/jquery.min.js",
                        "~/vendor/bootstrap/js/bootstrap.bundle.min.js",
                        "~/vendor/jquery-easing/jquery.easing.min.js",
                        "~/vendor/magnific-popup/jquery.magnific-popup.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Vendoring/css").Include(
                      "~/vendor/fontawesome-free/css/all.min.css",
                      "~/vendor/magnific-popup/magnific-popup.css"));

            bundles.Add(new StyleBundle("~/Contenting/css").Include(
                      "~/Content/creative.min.css"));
        }
    }
}
